clear;
close all force;

load_file= false;
if(load_file)
    load('matlab.mat');    
else    
    %high res    
    kappas = 0:0.025:1;
    xvals = 6:-1:2;
    kappas_print = 0:0.1:1;
    SIZE = 1000;
    MONTE_CARLO = 7;
    sigma = 1;
    betta=1;
    %low res
%     kappas = 0:0.05:1;
%     xvals = [4];
%     kappas_print = kappas;
%     SIZE = 700;
%     MONTE_CARLO = 4;
%     sigma = 1;
%     betta=1;
    
    resultsEstEmpirical = zeros(1,numel(kappas));
    %resultsEstEmpirical2 = zeros(1,numel(kappas));

    %resultsTheo = zeros(1,numel(kappas));
    X = diag([xvals zeros(1,SIZE-numel(xvals))]);

    for ik = 1:numel(kappas)
        k = kappas(ik);%detectable = final_sigma*(betta^(1/4))/(kappa);
        %bulk_edge = sqrt(k*sigma^2)*(1+sqrt(betta));
        %fs =  sqrt((k)*sigma^2);
        %xs = (fs)*sqrt(  (1+betta+sqrt(1+14*betta+betta^2))/2);
        %lambda_of_xs = fs * sqrt( (xs/fs + fs/xs)*(xs/fs + betta*fs/xs));
        %resultsTheo(ik)= numel(xvals(xvals <= lambda_of_xs));
        for iter = 1 : MONTE_CARLO
            [m,n]=size(X);
            Ux = zeros(m,m);
            Vx = zeros(n,n);
             ob = orth(rand(m));
             Ux(:,1:numel(xvals)) = ob(:,1:numel(xvals));
             ob = orth(rand(n));
             Vx(:,1:numel(xvals)) = ob(:,1:numel(xvals));
%             Ux(:,1) = randn(m,1);
%             Ux(:,1) = Ux(:,1) / norm(Ux(:,1));
%             Vx(:,1) = randn(n,1);
%             Vx(:,1) = Vx(:,1) / norm(Vx(:,1));
            Z = normrnd(0,1,m,n);
            Y = Ux*X*Vx' + sigma.* Z/sqrt(n);
            R = binornd(1,1-k,m,n);
            Y(R == 1) = 0;
            [Uy,Dy,Vy] = svd(Y);
            Dy = diag(Dy);
           
            %resultsDetect(ik) = numel(Dy(Dy > bulk_edge+0.01/k));
            
            fs =  sqrt((k)*sigma^2);
            xs = (fs)*sqrt(  (1+betta+sqrt(1+14*betta+betta^2))/2);
            lambda_of_xs = fs * sqrt( (xs/fs + fs/xs)*(xs/fs + betta*fs/xs));
            
            Dy(Dy <= lambda_of_xs)=0;
            y_hat = Uy*diag(Dy)*Vy';
            
            %Is it better to take the first sval than the 0 estimator?
%             if norm(Ux*X*Vx','fro') > norm(Ux*X*Vx'-y_hat,'fro')
%                 %if not, then the result is zero
%                 %if so, we'll call it Estimable
%                 resultsEstEmpirical2(ik) = resultsEstEmpirical2(ik) + 1;
%             end
            
            min = 1000000; %or any really high number
            for i = 0:numel(xvals)
                Dy_empir = Dy;
                Dy_empir(i+1:end) = 0;
                y_hat = Uy*diag(Dy_empir)*Vy';                
                frobloss=norm(Ux*X*Vx'-y_hat,'fro');
                if min > frobloss 
                    min = frobloss;
                    imin = i;
                end
            end
            resultsEstEmpirical(ik) = resultsEstEmpirical(ik) + imin;
        end
        resultsEstEmpirical(ik) = round(resultsEstEmpirical(ik)/MONTE_CARLO);
        %resultsEstEmpirical2(ik) = (resultsEstEmpirical2(ik)/MONTE_CARLO);

        fprintf('kappa = %d\n',k);
    end   
end


f= figure();
a = axes('Parent',f);%,'position',[0.13 0.64  0.35 0.25], 'ButtonDownFcn', @(s,e) run_svd_signal(e,axbottomLeft,kappa_ctrl,beta_control,sigma_control));
stairs(kappas,resultsEstEmpirical,'LineWidth',2);
grid on;
axis tight;
hold on;
title(a,'Estimability limit for singular values' );
xlabel('$\kappa$','Interpreter','LaTex','FontSize',15) 
ylabel(a,'Number of estimable singular values');
y1=get(gca,'ylim');
for iter = 1:numel(xvals)
    x = xvals(iter);
    min_kappa_of_sval= ( (1/x) * sigma * sqrt( (1+betta+sqrt(1+14*betta+betta^2))/2))^2;
    plot([ min_kappa_of_sval min_kappa_of_sval],[0 y1(end)],'LineWidth',2);
end


% [detectable,estimable] = get_AMSE_expr(betta,sigma);
% figure;
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Detectable/Estimable');
% hold on;
% estimableY = double(subs(estimable,kappas));
% detectableY = double(subs(detectable,kappas));
% %plot(kappas,detectableY,'LineWidth',2);
% plot(kappas,estimableY,'LineWidth',2);



%hmo = HeatMap(resultsDetect,'RowLabels',xs,'ColumnLabels',kappas,'Colormap', colormap(bone));
% figure;
% imagesc(kappas,xvals,resultsDetect);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Detectable');
% hold on;
% estimableY = double(subs(estimable,kappas));
% detectableY = double(subs(detectable,kappas));
% plot(kappas,detectableY);
%
% figure;
% imagesc(kappas,xvals,resultsEstimable);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Estimable');
% hold on;
% plot(kappas,estimableY);
%
% figure;
% imagesc(kappas,xvals,resultsTheoretic);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Theoretic');
% hold on;
% plot(kappas,estimableY);
%
% resultsXOR = double(abs(resultsTheoretic-resultsEstimable) < 0.5);
% figure;
% imagesc(kappas,xvals,resultsXOR);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('abs(theo-empir)>0.5');
