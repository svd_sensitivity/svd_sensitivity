function [det] = is_detectable(Dy)
    num_svals = 10;
    distsv = zeros(num_svals,1);

    for i = 2:2+num_svals
        distsv(i-1) = Dy(i) - Dy(i+1);
    end
    
    if (mean(distsv) + std(distsv)*10 < Dy(1)-Dy(2))
        det = 1;
    else
        det = 0;
    end        
end