clear;
close all force;

load_file= true;
if(load_file)
load('matlab.mat');
else
    
%low res
    
% kappas = 0.01:0.05:1;
% xvals = 0:0.1:3;
% kappas_print = 0.01:0.05:1;
% SIZE = 200;
% MONTE_CARLO = 4;

%med res
kappas = 0.05:0.05:1;
xvals = 0:0.1:3;
kappas_print = 0.1:1:1;
SIZE = 700;
MONTE_CARLO = 4;

%running with high-res values
% kappas = 0.01:0.025:1;
% kappas_print = 0.0:0.1:1;
% xvals = 0:0.1:3;
% SIZE = 400;
% MONTE_CARLO = 15;


sigma = 1;
beta=1;
alpha = 0.05;
resultsDetect = zeros(numel(xvals),numel(kappas));
resultsEstimable = zeros(numel(xvals),numel(kappas));
%at which points (x,kappa), the formula predicts that the signal x will still be
%predictable.
mu_B = 0;
for ik = 1:numel(kappas)
    for ix = 1:numel(xvals)
        k = kappas(ik);%detectable = final_sigma*(betta^(1/4))/(kappa);
        %bulk_edge = sqrt(k*sigma^2)*(1+sqrt(beta));
        det_thresh = sqrt(k*sigma^2)*sqrt((-3/2 * log(4*sqrt(pi) * alpha/100 ))^(2/3)); 
        x = xvals(ix);
        X = diag([x zeros(1,SIZE)]);
        counter_d = 0;
        counter_p = 0;
        for i = 1 : MONTE_CARLO            
            [m,n]=size(X);            
            Ux = zeros(m,m);
            Vx = zeros(n,n);
            Ux(:,1) = randn(m,1);
            Ux(:,1) = Ux(:,1) / norm(Ux(:,1));
            Vx(:,1) = randn(n,1);
            Vx(:,1) = Vx(:,1) / norm(Vx(:,1));      
            Z = normrnd(mu_B,sigma*1/sqrt(n),m,n);
            Y = Ux*X*Vx' + Z;
            R = binornd(1,1-k,m,n);
            Y(R == 1) = 0;
            [Uy,Dy,Vy] = svd(Y);
            Dy = diag(Dy); 
            
            
            if ( Dy(1) >det_thresh)
                counter_d = counter_d +1;
            end
            %thresh
            
            fs =  sqrt((k)*sigma^2);
            xs = (fs)*sqrt(  (1+beta+sqrt(1+14*beta+beta^2))/2);
            lambda_of_xs = fs * sqrt( (xs/fs + fs/xs)*(xs/fs + beta*fs/xs));
            
            Dy(Dy <= lambda_of_xs)=0;
            y_hat = Uy*diag(Dy)*Vy';
            
            %Is it better to take the first sval than the 0 estimator?
            if norm(Ux*X*Vx','fro') > norm(Ux*X*Vx'-y_hat,'fro')
                %if so, we'll call it Estimable
                counter_p = counter_p+1;
            end
        end
        resultsDetect(ix,ik) = counter_d;        
        resultsEstimable(ix,ik) = counter_p;        
    end
    fprintf('kappa = %d\n',k);
end 

end
resultsDetect = resultsDetect(:,3:end);
resultsEstimable = resultsEstimable(:,3:end);


resultsAVG = (resultsDetect + resultsEstimable)/(2*MONTE_CARLO);
kappas = 0.15:0.05:1;

[detectable,estimable] = get_AMSE_expr(beta,sigma);
figure;
imagesc(kappas,xvals,resultsAVG);
set(gca,'YTick',0:0.2:3,'XTick',0.15:0.1:1,'Ydir','normal');
xlabel('$\kappa$','Interpreter','LaTex','FontSize',20) 
ylabel('$x$','Interpreter','LaTex','FontSize',20) 
title('Detectable/Estimable');
hold on;
estimableY = double(subs(estimable,kappas));
detectableY = double(subs(detectable,kappas));
plot(kappas,detectableY,'LineWidth',2,'Color','m');
plot(kappas,estimableY,'LineWidth',2);
colorbar('Ticks',[1/4-0.2,1/2,3/4+0.2],'TickLabels',{'unobservable','observable','estimatable'});


%hmo = HeatMap(resultsDetect,'RowLabels',xs,'ColumnLabels',kappas,'Colormap', colormap(bone));
% figure;
% imagesc(kappas,xvals,resultsDetect);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Detectable');
% hold on;
% estimableY = double(subs(estimable,kappas));
% detectableY = double(subs(detectable,kappas));
% plot(kappas,detectableY);
% 
% figure;
% imagesc(kappas,xvals,resultsEstimable);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Estimable');
% hold on;
% plot(kappas,estimableY);
% 
% figure;
% imagesc(kappas,xvals,resultsTheoretic);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('Theoretic');
% hold on;
% plot(kappas,estimableY);
% 
% resultsXOR = double(abs(resultsTheoretic-resultsEstimable) < 0.5);
% figure;
% imagesc(kappas,xvals,resultsXOR);
% colormap(bone);
% set(gca,'YTick',xvals,'XTick',kappas_print,'Ydir','normal');
% xlabel('kappa');
% ylabel('x');
% title('abs(theo-empir)>0.5');
